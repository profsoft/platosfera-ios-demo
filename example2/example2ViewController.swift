import UIKit
import RxSwift
import RxCocoa
import RxDataSources

final class FeedNewsProfileViewController: BaseViewController {

    var viewModel: FeedNewsProfileViewModel!
    var isScrollEnable: BehaviorRelay<Bool>!

    private lazy var topInset: CGFloat = 10
    private lazy var topBarHeight = UIApplication.shared.statusBarFrame.height +
        (navigationController?.navigationBar.frame.height ?? 0)

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.theme.backgroundColor = themed {$0.primaryBackgroundColor}
            tableView.contentInset = UIEdgeInsets(top: topInset,
                                                  left: 0,
                                                  bottom: topBarHeight,
                                                  right: 0)
        }
    }

    private let isVisible = BehaviorRelay<Bool>(value: false)

    var postSettingTapSubject: PublishRelay<Post>!
    let goToPostDetails = PublishRelay<Post>()
    let goToPostCommentsDetails = PublishRelay<Post>()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        bindSelf()
        setupTableDataSource()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        isVisible.accept(true)
        viewModel.input.goUpdate.accept(())
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        isVisible.accept(false)
    }

    private func bindSelf() {
        isScrollEnable
            .asObservable()
            .subscribe(onNext: { [unowned self] value in
                self.tableView.isScrollEnabled = value
            })
            .disposed(by: disposeBag)

        tableView.rx
            .contentOffset
            .withLatestFrom(isVisible) { ($0, $1) }
            .filter { $0.1 }
            .subscribe(onNext: { [unowned self] (value, _) in
                print(value)
                if value.y <= -self.topInset {
                    self.isScrollEnable.accept(false)
                }
            })
            .disposed(by: disposeBag)

        viewModel.input
            .shareTap
            .subscribe (onNext: { [weak self] feedEntity in
                if let feedEntity = feedEntity {
                    self?.shareText(feedEntity.textToShare())
                }
        })
        .disposed(by: disposeBag)
    }

    private func configureTableView() {
        tableView.do {
            $0.register(cellType: PostMarkerCell.self)
            $0.separatorStyle = .none
        }
    }

    private func setupTableDataSource() {
        let dataSource = RxTableViewSectionedAnimatedDataSource<FeedNewsProfileViewModel.SectionType>(configureCell: { [unowned self] _, _, _, item in
            guard let cell = PostMarkerCell.reusableInstance(forTableView: self.tableView) else { return UITableViewCell() }
                cell.configuration()
                cell.selectionStyle = .none
                return cell
        })

        viewModel.output.sections
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}
