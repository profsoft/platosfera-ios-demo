final class FeedNewsProfileViewModel: ViewModel {

    typealias SectionType = AnimatableSectionModel<String, Post>

    private let router: FeedNewsProfileRouter
    private let feedService: FeedService
    private let currentData: CurrentDataState

    private let sections = BehaviorRelay<[SectionType]>(value: [])

    private let disposeBag = DisposeBag()

    struct Input {
        let likesTap = PublishRelay<Int>()
        let otherUserDetailsTap = PublishRelay<User>()
        let shareTap = PublishRelay<FeedEntity?>()
        let goUpdate = PublishRelay<Void>()
    }

    let input = Input()

    struct Output {
        let sections: Driver<[SectionType]>
    }

    let output: Output

    private let myId: Int

    init(feedService: FeedService,
         router: FeedNewsProfileRouter,
         currentData: CurrentDataState) {
        self.feedService = feedService
        self.router = router
        self.currentData = currentData
        output = Output(sections: sections.asDriver(onErrorJustReturn: []))

        myId = DefaultHelper.getInt(key: .myUserId)
        bindInput()
        bindUpdateData()
    }

    private func bindInput() {
        input.likesTap
            .flatMapLatest { [unowned self] entityId -> Observable<Void> in
                self.feedService.likeEntity(entityId: entityId)
        }
        .subscribe(onNext: {})
        .disposed(by: disposeBag)

        input.otherUserDetailsTap
            .filter { [weak self] user in
                self?.myId != user.id
        }
        .subscribe(onNext: { [weak self] user in
            self?.router.showOtherUserDetails(user)
        })
            .disposed(by: disposeBag)
    }

    private func bindUpdateData() {
        currentData.updateSections
            .filter { $0 == .news }
            .flatMapLatest {  _ -> Observable<Void> in
                return Observable.of(Void())
        }
        .bind(to: input.goUpdate)
        .disposed(by: disposeBag)

        input.goUpdate
            .flatMapLatest { [unowned self] _ -> Observable<[FeedEntity]> in
                if self.currentData.currentProfileType.value == .otherProfile {
                    if let userId = self.currentData.currentUser.value?.id {
                        return self.feedService.loadFeedForUser(userId: userId)
                    }
                }
                return self.feedService.loadFeed()
        }
        .asObservable()
        .materialize()
        .map { event -> [SectionType] in
            guard case let .next(value) = event else { return [] }

            var posts = [Post]()
            value.forEach { feed in
                if let post = feed as? Post {
                    posts.append(post)
                }
            }

            return [SectionType(model: "first", items: posts)]
        }
        .bind(to: sections)
        .disposed(by: disposeBag)
    }
}

