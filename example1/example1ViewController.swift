import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Reusable

final class BestSearchViewController: UIViewController, IndicatorInfoProvider {

    var viewModel: BestSearchViewModel!
    private let disposeBag = DisposeBag()
    //......

    //@IBOutlets ....

    override func viewDidLoad() {
        super.viewDidLoad()
        configureDataSource()
        bindSelf()
    }

    private func bindSelf() {
        viewModel.output
            .openSharView
            .subscribe(onNext: { string in
                self.shareText(string)
            })
            .disposed(by: disposeBag)
    }
    
    //Other functions
}

extension BestSearchViewController {
    private func configureTableView() {
        tableView.do {
            $0.register(cellType: InterestsViewCell.self)
            $0.register(cellType: TimelinePostCell.self)
            $0.register(cellType: EventMarkerCell.self)
            $0.register(cellType: TimelineGoingUserCell.self)
            $0.register(cellType: TimelineCheckInUserCell.self)
            $0.separatorStyle = .none
            $0.delegate = self
        }
    }
    
    // configuration cell functions ...

    private func configureDataSource() {

        let dataSource = RxTableViewSectionedAnimatedDataSource<BestSearchViewModel.SectionType>(configureCell: { [unowned self] _, table, indexPath, item in
            switch item {
            case .interest(let interest):
                return self.configureInterestCell(table: table,
                                                  indexPath: indexPath,
                                                  interest: interest)
            case .event(let event):
                let cell = table.dequeueReusableCell(for: indexPath) as EventMarkerCell
                return self.configureEventCell(cell: cell, with: event)
            case .post(let post):
                return self.configurePostCell(table: table,
                                              indexPath: indexPath,
                                              post: post)
            case .goingUser(let user):
                return self.configureGoingUserCell(table: table,
                                                   indexPath: indexPath,
                                                   user: user)
            case .checkInUser(let user):
                return self.configureCheckInUserCell(table: table,
                                                     indexPath: indexPath,
                                                     user: user)
            }
        })

        viewModel.output.sections
            .do (onNext: { [unowned self] section in
                guard let places = section.first?.items else { return }
                self.emptyDataLabel.isHidden = !places.isEmpty
            })
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

