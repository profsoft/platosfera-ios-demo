import Foundation
import RxCocoa
import RxSwift
import RxDataSources

struct ModelInterestCategory {
    var idInterest: Int?
    var type: TypeCellInterest?
    var name: String?
    var color: UIColor?
    var isSelected: Bool?
}

final class BestSearchViewModel: {

    private let constDelayRequest = RxTimeInterval(1)

    private let router: BestSearchRouter
    private let extraParameters: ExtraSearchParameters

    typealias SectionType = AnimatableSectionModel<String, CellModelBestSearch>

    private let checkSearching: PublishRelay<String>
    private let exactFind: PublishRelay<String>
    private var isVisible = BehaviorRelay<Bool>(value: false)
    private var findingCategoriesBestSearch = BehaviorRelay<[String]>(value: [])
    private let interestsStream = BehaviorRelay<[InterestCategory]>(value: [])
    private let isVisibleInterestsCell = BehaviorRelay<Bool>(value: true)

    private let section = BehaviorRelay<[SectionType]>(value: [])
    private let currentFindCategories = BehaviorRelay<[SearchSectionEntity]>(value: [])
    private let disposeBag = DisposeBag()

    private let searchServices: SearchServices
    private let feedService: FeedService
    private let schedulerProvider: SchedulerProvider


    private let interestsSection = BehaviorRelay<[ModelInterestCategory]>(value: [])

    struct Input {
        let viewRedyForData = PublishRelay<Void>()
        let changeVisible = PublishRelay<Bool>()
        let search = PublishRelay<String>()
        let exactFind = PublishRelay<String>()
    }

    let input = Input()

    struct InputCellAction {
        let goToUserProfile = PublishRelay<User>()
        let goToEventDetails = PublishRelay<Event>()
        let goToPlaceDetails = PublishRelay<Place>()
        let goToPostDetails = PublishRelay<Post>()
        let commentButtonTap = PublishRelay<Post>()
        let deletePostTap = PublishRelay<Post>()
        let deletePost = PublishRelay<Post>()
        let tapOnInterests = PublishRelay<Interest>()
        let goToMap = PublishRelay<Event>()
        let goToMapWithCoordinate = PublishRelay<GeoLocation>()
        let checkInButtonTap = PublishRelay<Int>()
        let shareTap = PublishRelay<Liketable>()
        let likeTap = PublishRelay<Int>()
        let changeInterestAtIndex = PublishRelay<ModelInterestCategory>()
        let goFilterTap = PublishRelay<Void>()
        let updateFilterSelected = PublishRelay<[ModelInterestCategory]>()
        let goToEventDetailsPeople = PublishRelay<Event>()
    }

    let inputCellAction = InputCellAction()

    struct Output {
        let sections: Driver<[SectionType]>
        let openSharView = PublishRelay<String>()
    }

    let output: Output

    init(searchServices: SearchServices,
         feedService: FeedService,
         extraParameters: ExtraSearchParameters,
         schedulerProvider: SchedulerProvider,
         router: BestSearchRouter) {

        self.searchServices = searchServices
        self.feedService = feedService
        self.extraParameters = extraParameters
        self.schedulerProvider = schedulerProvider
        self.router = router
        self.output = Output(sections: section.asDriver(onErrorJustReturn: []))

        bindSearching()
        bindPagination()
        bindSelf()
        bindInputTapsTransitions()
        bindInputTaps()
        bindInput()
        binfGetInterests()

        BindFilterReload()
    }
}

// MARK: cell taps and transitions

private extension BestSearchViewModel {

    private func bindGoToPostDetail() {
        inputCellAction.goToPostDetails
            .subscribe(onNext: { [unowned self] post in
                self.router.goToPostDetails(post)
            })
            .disposed(by: disposeBag)
    }

    private func bindCommentsTap() {
        inputCellAction.commentButtonTap
            .subscribe(onNext: { [unowned self] post in
                self.router.goToPostDetails(post, transitionsFromPush: true)
            })
            .disposed(by: disposeBag)
    }

    private func bindGoToUserProfile() {
        inputCellAction.goToUserProfile
            .subscribe(onNext: { [unowned self] user in
                self.router.goToUserProfile(user)
            })
            .disposed(by: disposeBag)
    }

    private func bindGoToEventDetail() {
        inputCellAction.goToEventDetails
            .subscribe(onNext: { [unowned self] event in
                self.router.goToEventDetails(event)
            })
            .disposed(by: disposeBag)
    }

    private func bindGoToEventDetailsPeople() {
        inputCellAction.goToEventDetailsPeople
        .subscribe(onNext: { [unowned self] event in
            self.router.goToEventDetailsPeople(event)
        })
        .disposed(by: disposeBag)
    }

    private func bindGoToPlaceDetails() {
        inputCellAction.goToPlaceDetails
            .subscribe(onNext: { [unowned self] place in
                self.router.goToPlaceDetails(place)
            })
            .disposed(by: disposeBag)
    }

    private func bindTapOnInterest() {
        inputCellAction.tapOnInterests
            .subscribe(onNext: { [unowned self] interest in
                // do something ...
            })
            .disposed(by: disposeBag)
    }

    private func bindGoToMap() {
        inputCellAction.goToMap
            .subscribe(onNext: { [unowned self] event in
                self.router.goToMap(event)
            })
            .disposed(by: disposeBag)
    }

    private func bindGoToMapCoordinate() {
        inputCellAction.goToMapWithCoordinate
        .subscribe(onNext: { [unowned self] coordinates in
            self.router.goToMap(with: coordinates)
        })
        .disposed(by: disposeBag)
    }

    private func bindDeletePost() {
        inputCellAction.deletePost
            .flatMapLatest { [unowned self] post -> Observable<Void> in
                guard let id = post.id else { return Observable.error(CustomError.noData) }
                return self.feedService.deletePost(id: id)
        }
        .subscribe(onNext: { [unowned self] _ in
            self.input.viewRedyForData.accept(())

        })
            .disposed(by: disposeBag)
    }

    private func bindGoFilterTap() {
        inputCellAction.goFilterTap
            .asObservable()
            .subscribe { [unowned self] _ in
                self.router.goToFilterScreen(extraParameters: self.extraParameters)
        }
        .disposed(by: disposeBag)
    }

    private func bindInputTapsTransitions() {

        bindGoToPostDetail()
        bindCommentsTap()
        bindGoToUserProfile()
        bindGoToEventDetail()
        bindGoToEventDetailsPeople()
        bindGoToPlaceDetails()
        bindTapOnInterest()
        bindGoToMap()
        bindGoToMapCoordinate()
        bindDeletePost()
        bindGoFilterTap()
    }

    private func bindGetArrayOfInterests() {
        searchServices.getArrayInterests()
            .asObservable()
            .materialize()
            .map { event -> [Interest] in
                guard case let .next(value) = event else { return [] }
                return value
        }
        .unwrap()
        .map { interests in
            var interestsCategory = [ModelInterestCategory]()
            interests.forEach { interest in
                interestsCategory.append(ModelInterestCategory(idInterest: interest.id,
                                                               type: .anyCell,
                                                               name: interest.name,
                                                               color: interest.category?.color,
                                                               isSelected: false))
            }
            return interestsCategory
        }
        .bind(to: interestsSection)
        .disposed(by: disposeBag)
    }

    private func bindUpdateInterests() {
        extraParameters.updateInterests
            .asObservable()
            .subscribe (onNext: { [unowned self] _ in
                // do something ...
            })
            .disposed(by: disposeBag)
    }

    private func bindChangeInterestsAtIndex() {
        inputCellAction.changeInterestAtIndex
            .flatMapLatest { [unowned self] interest -> Observable<[ModelInterestCategory]> in
                // do something ...
                return Observable.just(interests)
        }
        .bind(to: interestsSection)
        .disposed(by: disposeBag)
    }

    private func binfGetInterests() {

        bindGetArrayOfInterests()
        bindUpdateInterests()
        bindChangeInterestsAtIndex()

    }

    private func bindInputTaps() {
        inputCellAction.checkInButtonTap
            .asObservable()
            .switchMap({ [unowned self] entityId -> Observable<Void> in
                self.feedService.markAsGoing(entityId: entityId)
            })
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribeNext { _ in
                //todo
        }
        .disposed(by: disposeBag)

        inputCellAction.shareTap
            .asObservable()
            .subscribeNext { [unowned self] event in
                self.output.openSharView.accept(event.textToShare())

        }
        .disposed(by: disposeBag)

        inputCellAction.likeTap
            .asObservable()
            .switchMap({ [unowned self] entityId -> Observable<Void> in
                self.feedService.likeEntity(entityId: entityId)
            })
            .subscribeOnDefaultOr(self.schedulerProvider.io())
            .observeOnDefaultOr(schedulerProvider.main())
            .subscribeNext { _ in
                //todo
        }
        .disposed(by: disposeBag)
    }
}

// MARK: Binding default functional

extension BestSearchViewModel {

    private func bindSearching() {
        checkSearching.asObservable()
            .throttle(constDelayRequest, scheduler: MainScheduler.instance)
            .withLatestFrom(isVisible) {($0, $1)}
            .filter { $0.1 }
            .flatMapLatest { [unowned self] (query, _) -> Observable<[SearchSectionEntity]> in
                
                //some code .....
                
                return // some observable ...
        }
        .materialize()
        .map { event -> [SearchSectionEntity] in
            guard case let .next(value) = event else { return [] }
            return value
        }
        .unwrap()
        .map { categories -> [String] in
            var result = [String]()
            for element in categories {
                switch element {
                case .post(let post):
                    result.append(post.description ?? "")
                case .event(let event):
                    result.append(event.title ?? "")
                case .goingUser(let user):
                    result.append(user.user?.name ?? "")
                case .checkInUser(let user):
                    result.append(user.user?.name ?? "")
                case .defaultValue:
                    continue
                }
            }
            return result
        }
        .bind(to: findingCategoriesBestSearch)
        .disposed(by: disposeBag)
    }

    private func bindPagination() {
        goPagination.asObservable()
            .throttle(constDelayRequest, scheduler: MainScheduler.instance)
            .flatMapLatest { [unowned self] _ -> Observable<[SearchSectionEntity]> in
                
                // fetch data form request

        }
        .asObservable()
        .materialize()
        .map { [unowned self] event -> [SearchSectionEntity] in
            guard case let .next(value) = event else {
                if self.emptySearchFlag.value {
                    return self.prevPaginationArray
                } else {
                    return self.prevPaginationArrayForExactWord
                }
            }

            // do something with data
        }
        .bind(to: currentFindCategories)
        .disposed(by: disposeBag)
    }

    private func bindSelf() {
        input.viewRedyForData
            .bind(to: goPagination)
            .disposed(by: disposeBag)

        Observable.combineLatest(interestsSection, currentFindCategories) { (interest, category) in
            var mainArray = [CellModelBestSearch]()
            mainArray.append(CellModelBestSearch.interest(interest))
            for element in category {
                switch element {
                case .post(let post):
                    mainArray.append(CellModelBestSearch.post(post))
                case .event(let event):
                    mainArray.append(CellModelBestSearch.event(event))
                case .goingUser(let user):
                    mainArray.append(CellModelBestSearch.goingUser(user))
                case .checkInUser(let user):
                    mainArray.append(CellModelBestSearch.checkInUser(user))
                case .defaultValue:
                    continue
                }
            }
            return [SectionType(model: "firstSection", items: mainArray)]
        }
        .bind(to: section)
        .disposed(by: disposeBag)
    }

    private func bindInput() {
        input.changeVisible
            .bind(to: isVisible)
            .disposed(by: disposeBag)

        exactFind.asObservable()
            .throttle(constDelayRequest, scheduler: MainScheduler.instance)
            .flatMapLatest { [unowned self] query -> Observable<[SearchSectionEntity]> in
                
                //do something ...

                return // return some observable ...
        }
        .do(onNext: { [unowned self] items in
            // do something ...
        })
            .bind(to: currentFindCategories)
            .disposed(by: disposeBag)
    }
}

// MARK: feedService

extension BestSearchViewModel {

    private func likeEntity(entityId: Int) -> Observable<Void> {
        return feedService.likeEntity(entityId: entityId)
    }

    private func markAsGoing(entityId: Int) -> Observable<Void> {
        return feedService.markAsGoing(entityId: entityId)
    }

    private func markShared(id: Int) -> Observable<Void> {
        return feedService.markShared(id: id)
    }

}

