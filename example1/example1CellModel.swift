import Foundation
import RxDataSources

enum CellModelBestSearch {
    case interest([ModelInterestCategory])
    case event(Event)
    case post(Post)
    case goingUser(GoingUser)
    case checkInUser(CheckInUser)
}

extension CellModelBestSearch: Equatable {
    static func == (lhs: CellModelBestSearch, rhs: CellModelBestSearch) -> Bool {
        return lhs.identity == rhs.identity
    }
}

extension CellModelBestSearch: IdentifiableType {
    var identity: Int {
        switch self {
        case .event(let event):
            return event.identity
        case .post(let post):
            return post.identity
        case .goingUser(let goingUser):
            return goingUser.identity
        case .checkInUser(let checkInUser):
            return checkInUser.identity
        case .interest(let interests):
            return interests.count
        }
    }
}

extension Event: IdentifiableType {
    var identity: Int {
        let id = self.id ?? 0
        // .... next properties
        return id ^ address.hashValue ^ time.hashValue
    }
}

extension GoingUser: IdentifiableType {
    var identity: Int {
        let id = self.id ?? 0
        // .... next properties
        return id ^ address.hashValue ^ name.hashValue ^ n.hashValue
    }
}

extension CheckInUser: IdentifiableType {
    var identity: Int {
        let id = self.id ?? 0
        // .... next properties
        return id ^ address.hashValue ^ name.hashValue ^ contact.hashValue ^ addressPlace.hashValue
    }
}
