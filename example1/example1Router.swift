import Foundation

final class BestSearchRouter {

    weak var viewController: BestSearchViewController?

    init(viewController: BestSearchViewController) {
        self.viewController = viewController
    }
}

extension BestSearchRouter {

    func goToPostDetails(_ post: Post, transitionsFromPush: Bool = false) {
        if let viewController = viewController {
            let postVC = PostDetailsAssembly.createViewController(post: post)
            postVC.setTransitionsFromPush(transitionFromPush: transitionsFromPush)
            viewController.navigationController?.pushViewController(postVC,
                                                                    animated: true)
        }
    }

    func goToUserProfile(_ user: User) {
        if let viewController = viewController {
            let userViewController = ProfileAssembly.createViewController(user: user, otherUser: true)
            viewController.navigationController?.pushViewController(userViewController, animated: false)
        }
    }

    func goToEventDetails(_ event: Event) {
        if let viewController = viewController {
            let eventVC = EventDetailsAssembly.createViewController(event: event)
            viewController.navigationController?.pushViewController(eventVC, animated: false)
        }
    }

    func goToEventDetailsPeople(_ event: Event) {
        if let viewController = viewController {
            let eventVC = EventDetailsAssembly.createViewController(event: event)
            eventVC.transitionsFromCommingPeopleView = true
            viewController.navigationController?.pushViewController(eventVC, animated: false)
        }
    }

    func goToPlaceDetails(_ place: Place) {
        if let viewController = viewController {
            let placeVC = PlaceDetailsAssembly.createViewController(place: place)
            viewController.navigationController?.pushViewController(placeVC, animated: false)
        }
    }

    func goToMap(_ event: Event) {
        guard let coordinates = event.coordinates else { return }
        if let viewController = viewController {
            let vc = PlaceOnMapAssembly.createViewController(with: coordinates)
            viewController.navigationController?.pushViewController(vc, animated: false)
        }
    }

    func goToMap(with coordinates: GeoLocation) {
        if let viewController = viewController {
            let vc = PlaceOnMapAssembly.createViewController(with: coordinates)
            viewController.navigationController?.pushViewController(vc, animated: false)
        }
    }

    func goToFilterScreen(extraParameters: ExtraSearchParameters) {
        if let viewController = viewController {
            let userViewController = BestSearchFiltersAssembly.createViewController(parameters: extraParameters)
            viewController.navigationController?.pushViewController(userViewController, animated: false)
        }
    }
}
